variable "location" {
  type = string
  default = "westeurope"
}

variable "cluster_name" {
  type = string
  default = "aks-project"
}